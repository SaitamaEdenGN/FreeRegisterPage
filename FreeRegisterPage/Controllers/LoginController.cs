﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using FreeRegisterPage.Models;

namespace EpicNos_Control_Panel.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]

        public string Sha512(string value)
        {
            SHA512 sha = SHA512.Create();
            byte[] data = sha.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sb.Append(data[i].ToString("x2"));
            }
            return sb.ToString();
        }

        public ActionResult Athorize(Account account)
        {
            using (testEntities db = new testEntities())
            {
                string pass = Sha512(account.Password);
                var userDetails = db.Account.Where(x => x.Name.Equals(account.Name) && x.Password.Equals(pass)).FirstOrDefault();
                if (userDetails == null)
                {
                    account.LoginErrorMessage = "Incorrect User Credentials.";
                    return View("Index", account);
                }
                else
                {
                    Session["userID"] = userDetails.AccountId;
                    Session["Name"] = userDetails.Name;
                    return RedirectToAction("Index", "Home");
                }
            }
        }

        public ActionResult LogOut()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Login");
        }
    }
}