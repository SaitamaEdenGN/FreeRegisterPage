﻿using FreeRegisterPage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FreeRegisterPage.Controllers
{
    public class RegisterController : Controller
    {
        [HttpGet]

        public ActionResult Index(int AccountId = 0, byte Authority = 0, string RegistrationIp = null, string VerificationToke = null, int ReferrerId = 0)
        {
            Account account = new Account();
            return View(account);
        }

        [HttpPost]
        public ActionResult Index(Account account)
        {
            using (testEntities db = new testEntities())
            {
                string pass = Sha512(account.Password);
                string pass2 = Sha512(account.ConfirmPassword);
                string password = account.ConfirmPassword;
                if (db.Account.Any(x => x.Name == account.Name))
                {
                    ViewBag.DuplicateMessage = "Username arledy exist.";
                    return View("AddOrEdit", account);
                }
                account.Password = pass;
                account.ConfirmPassword = pass;
                db.Account.Add(account);
                db.SaveChanges();
            }
            ModelState.Clear();
            ViewBag.SuccessMessage = "Registration Successful.";
            return View("AddOrEdit", new Account());
        }

        public string Sha512(string value)
        {
            SHA512 sha = SHA512.Create();
            byte[] data = sha.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sb.Append(data[i].ToString("x2"));
            }
            return sb.ToString();
        }
    }
}