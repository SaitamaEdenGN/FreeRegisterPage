//------------------------------------------------------------------------------
// <auto-generated>
//     Codice generato da un modello.
//
//     Le modifiche manuali a questo file potrebbero causare un comportamento imprevisto dell'applicazione.
//     Se il codice viene rigenerato, le modifiche manuali al file verranno sovrascritte.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FreeRegisterPage.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public partial class Account
    {
        public long AccountId { get; set; }
        public short Authority { get; set; }
        [Required(ErrorMessage = "This field is required!")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [DisplayName("UserName")]
        [Required(ErrorMessage = "This field is required!")]
        public string Name { get; set; }
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "This field is required!")]
        public string Password { get; set; }
        [DisplayName("Confirm Password")]
        [Required(ErrorMessage = "This field is required!")]
        [Compare("Password")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
        public string RegistrationIP { get; set; }
        public string VerificationToken { get; set; }
        public long ReferrerId { get; set; }

        public string LoginErrorMessage { get; set; }
    }
}
